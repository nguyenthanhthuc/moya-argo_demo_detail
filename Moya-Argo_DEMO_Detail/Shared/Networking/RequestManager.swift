//
//  RequestManager.swift
//  Moya-Argo_DEMO_Detail
//
//  Created by iOS Developer on 5/29/17.
//  Copyright © 2017 Nguyen Thanh Thuc. All rights reserved.
//

import UIKit

protocol RequestManagerType {
    //
    func requestDecodeable()
    
    //
    func requestVoid()
}

class RequestManager: RequestManagerType {
    
    func requestDecodeable() {
        
    }
    
    func requestVoid() {
        
    }
}
