//
//  RequestService.swift
//  Moya-Argo_DEMO_Detail
//
//  Created by iOS Developer on 5/29/17.
//  Copyright © 2017 Nguyen Thanh Thuc. All rights reserved.
//

import UIKit
import Moya


enum RequestService {
    case uploadToImagga()
    case dowloadImage
    case fetchRepository()
}

extension RequestService: TargetType {
    
    var baseURL: URL {
        return URL(string: "")!
    }
    
    var path: String {
        switch self {
        case .uploadToImagga():
            return "/v1/gifs"
        default:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .uploadToImagga():
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        
        switch self {
        case .fetchRepository():
            return ["sort": "pushed"]
        default:
            return [:]
        }
    }
    
    var sampleData: Data {
        switch self {
        case .uploadToImagga():
            return "fetch".utf8Encoded
        default:
            return "".utf8Encoded
        }
    }
    
    var task: Task {
        return .request
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var validate: Bool {
        return true
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}

