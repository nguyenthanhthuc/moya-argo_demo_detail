//
//  Constant.swift
//  Moya-Argo_DEMO_Detail
//
//  Created by iOS Developer on 5/30/17.
//  Copyright © 2017 Nguyen Thanh Thuc. All rights reserved.
//

import UIKit

struct Storyboards {
    static let home = "Home"
    static let authentication = "Authentication"
    static let converse = "Converse"
    static let analyse = "Analyse"
    static let mediaGallery = "MediaGallery"
    static let readyToPost = "ReadyToPost"
    static let leftMenuVC = "LeftMenuViewController"
    
    static let rightMenuVC = "RightMenuViewController"
}
